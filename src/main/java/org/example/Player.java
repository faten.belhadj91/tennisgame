package org.example;

public class Player {

    private int score = 0;
    private String name;

    boolean avantage = false;

    public Player(String name) {
        this.name = name;
    }

    public void addPoint() {
        this.score++;
    }

    @Override
    public String toString() {
        return "Player " + name + " : " + (score == 0 ? 0 : score == 1 ? 15 : score == 2 ? 30 : avantage ? "AD" : 40);
    }

    public int getScore() {
        return score;
    }

    public void setAvantage(boolean avantage) {
        this.avantage = avantage;
    }
}

