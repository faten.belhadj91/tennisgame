package org.example;

public class Player2 {

    private int score = 0;
    private String name;



    public Player2(String name) {
        this.name = name;
    }

    public void addPoint() {
        this.score++;
    }


    @Override
    public String toString() {
        return "Player2{" +
                "score=" + score +
                ", name='" + name + '\'' +
                '}';
    }

    public String showScore(Player2 adv) {

        return "Player " + name + " : " + (score == 0 ? 0 :
                score == 1 ? 15
                        : score == 2 ? 30
                        : score == 3 ? 40
                        : score > adv.getScore() ? "AD" : 40 );
    }

    public int getScore() {
        return score;
    }

}

