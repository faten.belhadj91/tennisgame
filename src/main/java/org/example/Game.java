package org.example;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Game {
    Player A = new Player("A");
    Player B = new Player("B");

    public void checkPoint(int index, String chaine) {
        if (index > chaine.length() - 1) {
            System.out.println("No winner found");
            return;
        }
        char c = chaine.charAt(index);
        if (c == 'A') {
            A.addPoint();
        } else {
            B.addPoint();
        }
        System.out.println(A + " / " + B);
        if (!checkScore(index + 1, chaine)) {
            checkPoint(index + 1, chaine);
        }
    }

    private boolean checkScore(int i, String chaine) {

        // si le score est 40 / 40 je splite le resultat en couple de chart
        // ex: (AB)(BA)(AB)(AA)
        // et je cherche le premier couple identique
        if (A.getScore() == B.getScore() && A.getScore() == 3) {
            for (int x = i; x < chaine.length() - 1; x += 2) {
                if (chaine.charAt(x) == chaine.charAt(x + 1)) {
                    System.out.println("Player " + chaine.charAt(x) + " wins the game");
                    return true;
                }
            }
        } else if (A.getScore() > 3 && B.getScore() < 3) {
            System.out.println("Player A wins the game");
            return true;
        }
        if (B.getScore() > 3 && A.getScore() < 3) {
            System.out.println("Player B wins the game");
            return true;
        }
        return false;
    }

    public void start(String input){
        if (!checkInput(input)) {
            return;
        }
        A = new Player("A");
        B = new Player("B");
        checkPoint(0, input);
    }

    private static boolean checkInput(String input) {
        String regex = "^[AB]*$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);

        if (!matcher.matches()) {
            System.err.println("Only 'A' and 'B' are accepted");
            return false;
        }
        return true;
    }

}
