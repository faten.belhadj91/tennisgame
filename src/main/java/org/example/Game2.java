package org.example;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Game2 {
    Player2 A = new Player2("A");
    Player2 B = new Player2("B");

    public void checkPoint(int index, String chaine) {
        if (index > chaine.length() - 1) {
            System.out.println("No winner found");
            return;
        }
        char c = chaine.charAt(index);
        if (c == 'A') {
            A.addPoint();
        } else {
            B.addPoint();
        }

        if (!checkScore()) {
            System.out.println(A.showScore(B) + " / " + B.showScore(A));
            checkPoint(index + 1, chaine);
        }
    }

    private boolean checkScore() {
        if (A.getScore() > 3 && B.getScore() < 3) {
            System.out.println("Player A wins the game");
            return true;
        }
        if (B.getScore() > 3 && A.getScore() < 3) {
            System.out.println("Player B wins the game");
            return true;
        }
        if ((B.getScore() > 3 || A.getScore() > 3) && Math.abs(A.getScore() - B.getScore()) >= 2) {
            if (B.getScore() > A.getScore()) {
                System.out.println("Player B wins the game");
            } else {
                System.out.println("Player A wins the game");
            }
            return true;
        }
        return false;
    }

    public void start(String input) {
        if (!checkInput(input)) {
            return;
        }
        A = new Player2("A");
        B = new Player2("B");
        checkPoint(0, input);
    }

    private static boolean checkInput(String input) {
        String regex = "^[AB]*$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);

        if (!matcher.matches()) {
            System.err.println("Only 'A' and 'B' are accepted");
            return false;
        }
        return true;
    }

}
