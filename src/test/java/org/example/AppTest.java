package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Unit test for simple App.
 */
public class AppTest {

    @Test
    void testPlayerAWins() {
        String input = "ABABAA";
        String expectedOutput = "Player A wins the game";

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));
        Game game = new Game();
        game.start(input);
        String actualOutput = outputStream.toString().trim();

        Assertions.assertTrue(actualOutput.contains(expectedOutput));
    }

    @Test
    void testPlayerBWins() {
        String input = "ABABABBBB";
        String expectedOutput = "Player B wins the game";

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));
        Game game = new Game();
        game.start(input);
        String actualOutput = outputStream.toString().trim();
        Assertions.assertTrue(actualOutput.contains(expectedOutput));
    }

    @Test
    void testAnyWinnerFounded() {
        String input = "ABAB";
        String expectedOutput = "No winner found";

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));
        Game game = new Game();
        game.start(input);
        String actualOutput = outputStream.toString().trim();

        Assertions.assertTrue(actualOutput.trim().contains(expectedOutput.trim()));
    }


    @Test
    void testCheckInputSequence() {
        String input = "JJJ";
        String expectedOutput = "Only 'A' and 'B' are accepted";

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setErr(new PrintStream(outputStream));
        Game game = new Game();
        game.start(input);
        String actualOutput = outputStream.toString().trim();

        Assertions.assertEquals(actualOutput.trim(), expectedOutput.trim());
    }
}
